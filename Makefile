all:
	cd src; make
	gitbook build . public

serve:
	cd src; make
	gitbook serve . public

pdf:
	cd src; make
	gitbook pdf . boxes.pdf

epub:
	cd src; make
	gitbook epub . boxes.epub

.PHONY: all
