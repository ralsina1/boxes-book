# Recapitulation

If you are here, you have lived through 11 clumsy attempts at teaching you
something. Or rather, to *show* you something.

What did we get for our trouble?

* Some ideas about how to lay down text on a page.
* A rough implementation of such ideas.
* A specific thing our implementation can't do (yet)
* An idea of how it *could* do it.

And, the real goal:

* A process.

What do I mean by "a process"? A way to work, to turn an idea into code.
Sure, our process seems to be limited to "write stuff down", but in truth
it's slightly more than that. Here it is in more detail:

Start with a silly version of what you want and make it work.

![lesson1.svg](lesson1.svg)

Keep a copy of that thing, and think of a way to make it less silly.

![lesson2.svg](lesson2.svg)

Iterate previous point until it sort of looks like what you *really* want, and
not silly. Always keep track of the improvement.

![lesson6.svg](lesson6.svg)

Try to make it do what you really want, and notice the problems and unstated
assumptions.

![lesson7.svg](lesson7.svg)

Iterate previous point until it starts doing what you want it to do.

![lesson7_pride_and_prejudice.svg](lesson7_pride_and_prejudice.svg)

Apply yourself to make it do things *well*

![lesson10.svg](lesson10.svg)

Iterate previous point until you reach a point of diminishing returns, where
improving the code is too hard.

We are now at **that** point, and the process needs a tweak. We need to
start doing things in a different way. Just hacking it won't cut it.

That is not a small thing. A vast majority of people who try to learn coding
on their own never get this far. And while in the rest of the book we will be
working on ways to make what we have now *better*, what we have is *fine*.

For many people this is as far as your coding needs to take you. You can just
take your knowledge of the language syntax, a desire to play with it and a
problem and ride those things into a working solution that will *do what you
need*.

I am being deadly serious here. It's possible that you can just stop here. In
fact, maybe you should go, grab the problem that has lead you into trying to
learn to code, and just *play for a while*.

Go, doodle, build something useful. Create a script that automates something
you want. Use the approach I showed you and just build something.

The rest of the book will still be here if you ever need to level up.

And if you are ready to go on, then let's go.
