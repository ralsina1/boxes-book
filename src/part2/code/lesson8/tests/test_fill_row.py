import pytest

import boxes


def test_fill_row():
    letters = [boxes.Box(letter="x", x=i, w=1) for i in range(100)]
    letters[90].letter = " "
    page = boxes.Box(x=0, y=0, w=30, h=30)
    row = boxes.fill_row(letters, page, 0)
    assert len(row) == 91
    assert len(letters) == 9
    assert row[-1].letter == " "


def test_fill_row_with_breaking_chars():
    letters = [boxes.Box(letter=" ", x=i, w=1) for i in range(100)]
    page = boxes.Box(x=0, y=0, w=30, h=30)
    row = boxes.fill_row(letters, page, 0)
    assert len(row) == 31
    assert len(letters) == 69


def test_fill_row_with_a_newline():
    letters = [boxes.Box(letter="x", x=i, w=1) for i in range(100)]
    letters[90].letter = "\n"
    page = boxes.Box(x=0, y=0, w=30, h=30)
    row = boxes.fill_row(letters, page, 0)
    assert len(row) == 91
    assert len(letters) == 9
    assert row[-1].letter == "\n"
