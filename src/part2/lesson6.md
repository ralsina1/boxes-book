# Boxes v0.17

In the [previous lesson](lesson5.html) we added tests for our `justify_row()`
function, allowing us to have some confidence that we will not break it if we
play with it.

In the lesson **before that** we complained that our `layout()` function was
too complex, and that was why we created `justify_row()` in the first place.

So, let's continue to take things out of `layout()`...

Look at these fragments of code:

```python-include-norun:../lesson5/boxes.py:129:147
```

```python-include-norun:../lesson5/boxes.py:150:155
```

What is happening is that we can `break_line` for two reasons, and thus are
setting a "flag" which we then use to do the actual line breaking. That is
because if we break because of a newline, we don't justify the line.

So, if we gave `justify_row()` the skill of handling lines ended in newline,
then that distinction goes away and layout() is simpler.

The good thing is that we already have a test for it! It's the one we marked
as "expected failure" in the previous lesson, so if we make it pass... we are
on the right path.

How do we want to handle it? Let's look at our test, and remember that *we are
defining the desired behavior there*.

```python-include-norun:../lesson5/tests/test_justify_row.py:55:73
```

It looks easy enough: just **don't** spread the slack if we are ending on a
newline. That change is simple:

```python-include-norun:boxes.py:59:71
```

And run the test suite.

```text
$ env PYTHONPATH=. pytest
============================= test session starts ==============================
platform linux -- Python 3.6.2, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: code/lesson6, inifile:
collected 5 items

tests/test_justify_row.py ..X..                                   [100%]

===================== 4 passed, 1 xpassed in 0.25 seconds ======================
```

And now our test "xpasses". That means we marked as "expected to fail" and it
unexpectedly passes.

So, we can remove that mark now, and the test suite passes. Let's change
`layout()` to make it do the right thing by removing code and changing the
`if` in line 133:

```python-include-norun:boxes.py:132:158
```

But ... we are changing `layout()`. And `layout()` has no tests. We need to
manually test it.

```bash
$ python boxes.py pride-and-prejudice.txt lesson6.svg
```

![lesson6.svg](lesson6.svg)

Oh, no, it's broken!

Can you guess what happened?

In the lines that end in newlines (like the first one), we are *still*
spreading the slack! That's because we are only adding the current box to the
row after we justify it, in line 158!

If we move that up, before the `if` that decides whether to break the line,
then it works:

```python-include-norun:../lesson6.1/boxes.py:129:138
```

```bash
$ python ../lesson6.1/boxes.py pride-and-prejudice.txt lesson6.1.svg
```

![lesson6.1.svg](lesson6.1.svg)

While we are here, let's change one more thing. Look at this code in
`justify_row()`:

```python-include-norun:boxes.py:76:88
```

* If we have `stretchies` then we make them wider.
* If we don't, then we move every box a little to the right.

Why are we doing this, which is the same thing, "spreading the slack" in two
different ways?

No reason.

So, because we have tests for `justify_row()` we can make the code more
**uniform** and less complex.

```python-include-norun:../lesson6.1/boxes.py:76:85
```

```text
$ env PYTHONPATH=. pytest

============================= test session starts ==============================
platform linux -- Python 3.6.2, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: code/lesson6.1, inifile:
collected 5 items
tests/test_justify_row.py F..F.                                           [100%]

=================================== FAILURES ===================================
_____________________________ test_justify_simple ______________________________

[snip]
        # The last element should be flushed-right
>       assert row[-1].x + row[-1].w == page.x + page.w
E       assert (45.900000000000006 + 5.0) == (0 + 50)
E        +  where 45.900000000000006 = Box(45.900000000000006, 0, 5.0, 0, "a").x
E        +  and   5.0 = Box(45.900000000000006, 0, 5.0, 0, "a").w
E        +  and   0 = Box(0, 0, 50, 0, "x").x
E        +  and   50 = Box(0, 0, 50, 0, "x").w

tests/test_justify_row.py:27: AssertionError
_________________________ test_justify_trailing_spaces _________________________

[snip]
        # The last element should be flushed-right
>       assert row[-1].x + row[-1].w == page.x + page.w
E       assert (44.45 + 6.25) == (0 + 50)
E        +  where 44.45 = Box(44.45, 0, 6.25, 0, "a").x
E        +  and   6.25 = Box(44.45, 0, 6.25, 0, "a").w
E        +  and   0 = Box(0, 0, 50, 0, "x").x
E        +  and   50 = Box(0, 0, 50, 0, "x").w

tests/test_justify_row.py:93: AssertionError
====================== 2 failed, 3 passed in 0.21 seconds ======================
```

Annnnnd it fails horribly. That is **good**. What did you expect? That we
could just change the code and everything would be **fine?**

No, reader, that is not how this works. This is good because our tests
found a bug we just introduced. We went in with an ax, we broke
something. We now fix it. Hakuna matata, the circle of coding.

Turns out the problem was a bug in our tests. We were creating the rows
like this:

```python-norun
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    page = boxes.Box(w=50, h=50)
    separation = .1
```

And that is wrong, because we are saying they have a `separation` of .1 and
**are not separating them**.

The fix is easy:


```python-include-norun:../lesson6.2/tests/test_justify_row.py:9:13
```

You think about it :-)

And, does it look good in real life?

```bash
$ python ../lesson6.2/boxes.py pride-and-prejudice.txt lesson6.2.svg
```

![lesson6.2.svg](lesson6.2.svg)

------

* Full source code for this lesson <a href="../lesson6.2/boxes.py.html" target="_blank">boxes.py</a>
* <a href="code/diffs/lesson6_diff.html" target="_blank">Difference with code from last lesson</a>
