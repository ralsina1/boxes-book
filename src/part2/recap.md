# Recapitulation

Still around? After 22 whole sections of me explaining things? Congratulations
on your persistence!

What is it I hope you gained from that effort?

* A better idea ahout how to lay down text on a page.
* A decent implementation of that idea.
* Some concepts of how to write tests for your code.
* A better understanding of how to develop a piece of software.

Because yes, again, what I have really been explaining is not a piece of code,
but **a process.**

What is that process?

1. Started with a haphazard collection of code fragments. To know how to
   create haphazard collections of code fragments, read part 1 of this book.
2. Turn it into a usable script.
3. Fix obvious problems in the code.
4. Refactor, for example:
   * Splitting long functions into smaller, easier ones.
   * Rewriting code in better ways.
   * Improve readability of your code.
5. Write tests, but use manual tests if needed.
6. If tests fail, fix the code.
7. If tests pass, go back to refactoring.

Every once in a while, instead of fixing things or refactoring, you
want to implement a new feature. Do that, too.

This process is ongoing, and may never stop as long as the software is being
actively used and will serve you well as long as you are the only person
coding in a project. Once you are part of a team, you will need to make some
changes.

Another thing that changes once a project is useful is that it will start
having people use it for real. And no amount of testing one does is a match
for a single, determined user who is not the author of the code.

That is what the next part of the book is about:

* Cooperation
* Usability
* Continuous improvement
* Handling users
* Communication
* Documentation

It's going to be light in coding and heavy on "other things". In fact it's
about a ton of things that are **not code**.

BUT!

You may not need that. It's perfectly possible that this is **enough.** If you
are coding for yourself, maybe this is all you need. If you are a scientist,
then maybe this is as far as you need to go. I think the things I will explain
in part 3 are useful, but they are far from necessary.

You may want to stop now, go stretch your legs coding your own project for a
while and come back in a few months with a nice, useful pile of code. If so,
**please do it**. This book is not going anywhere, and you can always followup
later.

I think part 3 is useful, and I hope to see you there.