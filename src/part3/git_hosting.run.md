# Git Hosting (GitLab and GitHub)

In the previous chapter we covered *local* usage of Git. We learned how to
keep our code versioned using it, how to go back in time, how to undo changes,
how to keep separate *branches* of our code to work in more than one thing at
a time, and how to move changes from one branch to another. That is a lot!

In this chapter we will continue doing more or less the same thing, but with a
twist: how about having branches that don't live in your own computer?

If you worked in a project with someone else, you could each work in your own
copy of the repo, and then sync changes with each other!

Or, if there was a company that provided the service, there could be a
"central" copy of your repository, and you could both sync your codes with it,
and then no matter how many people worked on the project, **all of them**
could cooperate!

Good news! There are a number of companies that provide that service. For
free! I am going to focus on [GitLab](https://gitlab.com/) mostly because they provide
not only a service to give you public repositories of your projects for free,
but also **private** ones for things you don't want to share.

Another good choice is [GitHub](https://github.com/) -- their platform is the
most popular, hosting projects from many communities, in many languages, and
frameworks. GitHub is a great place to go explore, and to find contributors.
The only drawback is that private repos are not free. Yet another alternative
is [BitBucket](https://bitbucket.org/).

When I am giving specific instructions, I will give alternatives for GitLab
and GitHub. They are very, very similar.

## Remote Repositories

Until now you have been using what is called a *local* git repository. The
most important thing a git hosting service gives you is a *remote* repository.
It's basically the same thing, with branches and commits and all that, but in
someone else's machine.

What can you do with a remote repository?

* "push" changes to it. You change something locally, then "push" it to the
  server, and now the server has a copy.
* "pull" changes from it. If someone pushed a change to the server, you can
  "pull" it from the server into your local repository.

When you combine those two things, you can **collaborate**. Suppose there are
two persons developing a project. One creates a change, **pushes** it to a
common remote repository, then the other **pulls** it into his own local
repository, and voilá, they are both working on the same code!

## Preparation

Create an account for yourself. Mine is `ralsina` so I will use that in the
examples, you use your own!

The details on how to setup the account I leave to you, they are no different to any other online service. Except for SSH keys.

All the communication between you and the site is private and encrypted. To do
that, you need to give the site your "public key". This also allows the site
to *know* you are who you say you are. You can *sign* your code using your keys.

So, if you have a `~/.ssh/id_rsa.pub` file, good. If you don't you can create one
with this command:

```sh
ssh-keygen -t rsa -C "your_email@example.com"
```

And then that will create the `~/.ssh/id_rsa.pub` file. That file is your **public key.** There is also one without the `.pub` extension. That is your **private key**. **NEVER SHARE THAT ONE WITH ANYONE. EVER EVER.**

**REALLY NEVER EVER SHARE THAT ONE.**

Open that public file in a text editor and copy its contents.

<dl>
<dt>On GitLab:</dt>
    <dd>Top-right user menu ▶ Settings ▶ SSH keys ▶ Paste the public key.</dd>
<dt>On GitHub:</dt>
    <dd>Top-right user menu ▶ Settings ▶ SSH and GPG keys ▶ New SSH key ▶ Paste the public key.</dd>
</dl>

## Creating a repository and pushing to it

Creating a new repository your project is simple. Go to:

<dl>
<dt>On GitLab:</dt>
    <dd>Top plus sign menu ▶ New project</dd>
<dt>On GitHub:</dt>
    <dd>Top plus sign menu ▶ New repository</dd>
</dl>

Then, you can pick a name for your new project and enter a description. Since
we've already created our Git repository and added commits in the previous
chapter, you should start from a blank slate (without any README files, without
using templates).


Now, you can tell Git about the location of your remote repository. Grab the
SSH URL displayed on the repository page, and in your existing repository, do
this:

```bash
|||1,2
$ git remote add git@gitlab.com:ralsina/my-new-repo.git
$ git push -u origin master
```

That `-u` switch means that `git push` and `git pull` will operate on the
`origin/master` branch when you're on `master` locally.

After `git push` completes, you can refresh the browser and see your changes.

