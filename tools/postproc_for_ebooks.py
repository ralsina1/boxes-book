#!/usr/bin/env python

import sys

import lxml.html
import lxml.etree
from lxml.cssselect import CSSSelector


def main(in_path, out_path):
    with open(in_path) as inf:
        dom = lxml.html.fromstring(inf.read())
    images = CSSSelector("img")(dom)
    for i in images:
        src = i.attrib["src"]
        if src.endswith(".svg"):
            i.attrib["src"] = src.replace(".svg", ".png")
    with open(out_path, "wb+") as outf:
        outf.write(lxml.etree.tostring(dom))


if __name__ == "__main__":
    main(*sys.argv[1:])
